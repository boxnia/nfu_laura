app.factory("FlashService", ['$rootScope', function ($rootScope) {
        console.log("factoria flashservice")
        var service = {};
        service.load_menu = load_menu;
        service.load_header = load_header;
        return service;
        function load_menu(value) {
            console.log(value)

            if (value.tipo === "normal") {

                $rootScope.user_l = true;
                $rootScope.user = false;
                //$rootScope.log($rootScope.menu.user)
            } else {

                $rootScope.user_log = false;
                $rootScope.user = true;
            }

        }
        ;

        function load_header(value) {
            console.log(value.avatar)

            if (value.tipo == "normal") {
                $rootScope.avatar = value.avatar;
                $rootScope.headerMain = false;
                $rootScope.headerRegister = false;
                $rootScope.headerUsu = true;
                $rootScope.headerAdmin = false;

                //$rootScope.log($rootScope.menu.user)
            } else if (value.tipo == "admin") {
                $rootScope.avatar = value.avatar;
                $rootScope.headerMain = false;
                $rootScope.headerRegister = false;
                $rootScope.headerUsu = false;
                $rootScope.headerAdmin = true;
            } else if (value == "main") {
                $rootScope.headerMain = true;
                $rootScope.headerRegister = false;
                $rootScope.headerUsu = false;
                $rootScope.headerAdmin = false;

            } else {
                $rootScope.headerMain = false;
                $rootScope.headerRegister = true;
                $rootScope.headerUsu = false;
                $rootScope.headerAdmin = false;
            }

        }


    }]);
/////////////////////////////////$cookie
app.factory("authService", ['$http', '$cookies', '$rootScope',
    function ($rootScope, $cookies, $http) {
       
        var service = {};

        service.SetCredentials = SetCredentials;
        service.ClearCredentials = ClearCredentials;
        return service;



        function SetCredentials(user) {
            console.log(user);
            var user=JSON.stringify(user);
            var authdata = Base64.encode(user);
            var now = new Date(),
                    // this will set the expiration to 1 day
                    exp = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);

            //$http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
            $cookies.putObject('user', authdata);
        }

        function ClearCredentials() {
           // $rootScope.globals = {};
            $cookies.remove('user');
           // $http.defaults.headers.common.Authorization = 'Basic';
        }
    }]);

// Base64 encoding service used by AuthenticationService
var Base64 = {
    keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',
    encode: function (input) {
        var output = "";
        var chr1, chr2, chr3 = "";
        var enc1, enc2, enc3, enc4 = "";
        var i = 0;

        do {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
                    this.keyStr.charAt(enc1) +
                    this.keyStr.charAt(enc2) +
                    this.keyStr.charAt(enc3) +
                    this.keyStr.charAt(enc4);
            chr1 = chr2 = chr3 = "";
            enc1 = enc2 = enc3 = enc4 = "";
        } while (i < input.length);
        return output;
    },
    decode: function (input) {
        var output = "";
        var chr1, chr2, chr3 = "";
        var enc1, enc2, enc3, enc4 = "";
        var i = 0;

        // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
        var base64test = /[^A-Za-z0-9\+\/\=]/g;
        if (base64test.exec(input)) {
            window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
        }
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        do {
            enc1 = this.keyStr.indexOf(input.charAt(i++));
            enc2 = this.keyStr.indexOf(input.charAt(i++));
            enc3 = this.keyStr.indexOf(input.charAt(i++));
            enc4 = this.keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

            chr1 = chr2 = chr3 = "";
            enc1 = enc2 = enc3 = enc4 = "";
        } while (i < input.length);
        return output;
    }
};