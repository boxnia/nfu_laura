function paint(dataString) {
    $("#message_alert").html(dataString).fadeIn("slow");

    setTimeout(function () {
        $("#message_alert").fadeOut("slow")
    }, 5000);

    //reset the form
    $('#form_contact')[0].reset();

    // hide ajax loader icon
    $('.ajaxLoader').fadeOut("fast");

    // Enable button after processing
    $('#submitbtn').attr('disabled', false);

    /*if (dataString == "<div class='alert alert-success'>Your message has been sent </div>"){
     alert(dataString);
     }else{
     alert(dataString);
     }*/
}


$(document).ready(function () {



    var myLatlng = new google.maps.LatLng(38.822649, -0.608808);
    
    var mapOptions = {
        zoom: 14,
        center: myLatlng
    }
    
    
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Ubicaion NFU',
         icon: 'http://51.254.117.90/NFU/view/img/iconlog.png' 
    });
var contentString = '<div id="bodyContent">'+'<p class="infowindows" >Soterrani de les idees</p></br><a class="infowindows" target="_blank" href="http://maps.google.com/?q=38.822649,-0.608808">'+
      'Click para ver como llegar</a>'+  '</div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });
  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
    $(function () {
        $('#submitbtn').attr('disabled', false);
    });

    $("#form_contact").validate({
        rules: {
            inputName: {
                required: true
            },
            inputEmail: {
                required: true,
                email: true
            },
            inputMessage: {
                required: true
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group-contact').removeClass('success').addClass('error');
        },
        success: function (element) {
            $(element).closest('.form-group-contact').removeClass('error').addClass('success');
            $(element).closest('.form-group-contact').find('label').remove();
        },
        errorClass: "help-inline"
    });

    $("#submitbtn").click(function () {
        if ($("#form_contact").valid()) {

            // Disable button while processing
            $('#submitbtn').attr('disabled', true);

            // show ajax loader icon
            $('.ajaxLoader').fadeIn("fast");

            var dataString = $("#form_contact").serialize();
            $.ajax({
                type: "POST",
                /*url: "index.php?module=contact&function=check_contact",*/
                url: "/contact/check_contact",
                data: dataString,
                success: function (dataString) {
                    paint(dataString);
                }
            })
                    .fail(function () {
                        paint("<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>");
                    });
        }
        return false;
    });
});
