/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Vamos con este javascript a mostrar todos los prodcutos.
 */
/*////////////////////////////////////////////////////////////////*/ 
function load_game_ajax() {
    $.ajax({	   
            type: 'POST',	    
            url: "/register_games/games/load_game",
            params:{"load_game":true},	    
	    async: false
	}).success(function (data) {
	    var json = JSON.parse(data);
	    alert(json.msje);       	
	    pintar_game(json);	        
	}).fail(function (xhr) {
	    alert(xhr.responseText);
	});
}

/*//////////////////////////////////////////////////////////////// */
function load_game_get_v1() {
    /*$.get("modules/formulary/controller/controller_formulary.class.php?load=true", function(data, status){*/
    /*$.get("index.php?modules=formulary&function=load&load=true", function(data, status){*/
    
    $.post("/register_games/games/load_game", {"load_game":true}, function(data, status){
        var json = JSON.parse(data);
        /*$( "#content" ).html( json.msje );*/
        /*alert("Data: " + json.user.usuario + "\nStatus: " + status);*/
        
        pintar_prod(json);
    });
}

/*//////////////////////////////////////////////////////////////// */
function load_game_get_v2() {
    /*var jqxhr = $.get( "modules/formulary/controller/controller_formulary.class.php?load=true", function(data) {*/
    var jqxhr = $.get( "index.php?modules=formulary&function=load&load=true", function(data) {
        var json = JSON.parse(data);
        pintar_prod(json);
        /*alert( "success" );*/
    }).done(function() {
        /*alert( "second success" );*/
    }).fail(function() {
        /*alert( "error" );*/
    }).always(function() {
        /*alert( "finished" );*/
    });

    jqxhr.always(function() {
        /*alert( "second finished" );*/
    });
}

$(document).ready(function () {    
    /*load_game_ajax();*/
    load_game_get_v1();
    /*load_prod_get_v2();*/
});

function pintar_game(data) {
    /*alert(data.user.avatar);*/
    var content = document.getElementById("content");
    var div_game = document.createElement("div");
    var parrafo = document.createElement("p");

    var msje = document.createElement("div");
    msje.innerHTML= "msje = ";
    msje.innerHTML+= data.msje;
    /*#gamename,#time_start,#duration,#price_cash,#day_game,#places_number*/
    /*{"name": name, "time": time, "duration": duration, "pricecash": pricecash, "places": places, "day":day, "sport":sport}*/
    
    var name = document.createElement("div");
    name.innerHTML= "El nombre de la partida es = ";
    name.innerHTML+= data.game.name;
    
    var time = document.createElement("div");
    time.innerHTML= "La hora de la partida es = ";
    time.innerHTML+= data.game.time;
    
    var duration = document.createElement("div");
    duration.innerHTML= "La duración de la partida será de = ";
    duration.innerHTML+= data.game.duration;
    
    var pricecash = document.createElement("div");
    pricecash.innerHTML= "El precio de la entrada será = ";
    pricecash.innerHTML+= data.game.pricecash;
    
    var places = document.createElement("div");
    places.innerHTML= "El lugar de la patida será en = ";
    places.innerHTML+= data.game.places;
    
    var day = document.createElement("div");
    day.innerHTML= "La fecha de la partida será = ";
    day.innerHTML+= data.game.day;
    
    var sport = document.createElement("div");
    sport.innerHTML= "El deporte a practicar es = ";
    sport.innerHTML+= data.game.sport;
        
       
    
    /*var cad = data.prod.avatar;    
    var img = document.createElement("img");
    img.setAttribute("src", cad);
    img.setAttribute("height", "75");
    img.setAttribute("width", "75");
    img.setAttribute("alt", "Avatar");*/
   
    
    div_game.appendChild(parrafo);
     
    parrafo.appendChild(name);
    parrafo.appendChild(time);
    parrafo.appendChild(duration);
    parrafo.appendChild(pricecash);
    parrafo.appendChild(places);
    parrafo.appendChild(day);
    parrafo.appendChild(sport);    
    parrafo.appendChild(msje);
             
    content.appendChild(div_game);
    /*content.appendChild(img);*/
}


