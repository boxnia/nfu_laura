app.controller('UsersCtrl', function (FlashService, $cookies, $scope, $http, services, authService) {
    console.log("Estoy en users");
    $scope.myRegex_name = /^[a-zA-Z0-9](_(?!(\.|_))|\.(?!(_|\.))|[a-zA-Z0-9]){0,18}[a-zA-Z0-9]$/;
    $scope.myRegex_password = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    $scope.myRegex_email = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    //console.log($cookies.getObject('user'));


    var user_cookie = Base64.decode($cookies.getObject('user'));



    var data = JSON.stringify(user_cookie.token);

////////////////cargar datos modificar $cookie
    services.post('users', 'load_data_users', data)
            .then(function (data) {
                // console.log(data.data);
                if (data.data.success) {
                    authService.ClearCredentials('user');

                    authService.SetCredentials(data.data.users);
                    console.log($cookies.getObject('user'));
                    // console.log(data.data.nom_install[0]);
                    //$scope.customers = data.data;


                    ////////////
                } else {
                    // console.log($scope.messageFailure = data.data.type_error);
                }
            });

    // console.log($cookies.getObject('user'))
    var user_parse = Base64.decode($cookies.getObject('user'));
    var user=JSON.parse(user_parse);
   
///////////////////niveles radio y checkbox del form
    $scope.selectedValue = {
        id: 0
    };
    $scope.selectedValue_nivel = {
        id: 0
    };

    $scope.selectedValue_deportes = {
        id: 0
    };

    var todos = true;
    var futbol = true;

    var baloncesto = true;

    var voleibol = false;
    var tenis;
    var padel;
    if (user.todos == "1") {
        todos = true;

    }
    if (user.futbol == "1") {

        futbol = true;

    }

    if (user.baloncesto == 1) {


        baloncesto = true;

    }


    if (user.voleibol == "1") {


        voleibol = true;

    }
    if (user.tenis == "1") {

        tenis = true;

    }
    if (user.padel == "1") {

        padel = true;
    }

//////////////////////////////////llenar datos form
    $scope.profile = {
        nombre: user.nombre,
        password_old: "",
        password: "",
        password2: "",
        fecha_nac: user.fecha_nac,
        masculino: user.apellidos,
        femenino: user.apellidos,
        radii: [{
                id: 1,
                checked: user.masculino,
                name: "masculino"
            }, {
                id: 2,
                checked: user.femenino,
                name: "femenino"
            }],
        nivel: [{
                id: 1,
                checked: user.bajo,
                name: "bajo"
            }, {
                id: 2,
                checked: user.medio,
                name: "medio"
            }, {
                id: 3,
                checked: user.alto,
                name: "alto"
            }],
        deportes: [
            {
                id: 1,
                checked: todos,
                name: "todos"
            },
            {
                id: 2,
                checked: futbol,
                name: "futbol"
            },
            {
                id: 3,
                checked: baloncesto,
                name: "basket"
            },
            {
                id: 4,
                checked: voleibol,
                name: "volei"
            },
            {
                id: 5,
                checked: tenis,
                name: "tenis"
            },
            {
                id: 6,
                checked: padel,
                name: "padel"
            }
        ],
        //avatar: $cookies.getObject('user').nombre,

        pais: user.pais,
        provincia: user.provincia,
        ciudad: user.ciudad,
        telefono: user.numero,
        email: user.email,
        fecha_registro: user.fecha_registro
    }

//////////////////////////////activar menu y header depend $cookie
    if (user) {
        FlashService.load_header(user);
        FlashService.load_menu(user);
    } else {
        FlashService.load_header("");
        FlashService.load_menu("");
    }

    $scope.open_password = true;
//$cookies.getObject('user');
    $scope.profile.pais = null;
    $scope.paises = [];

    /* $http({
     method: 'GET',
     url: 'app_model/index.php?module=users&function=load_pais_users',
     
     data: {load_data: true}
     }).success(function (response) {
     console.log(response)
     $scope.paises = response;
     });*/


///////////////////////cargar country
    var data = {
        "load_data_pais": true, }
    services.post('users', 'load_pais_users', data)
            .then(function (data) {



                if (data.data === 'error') {
                    //  load_countries_v2("app_model/resources/ListOfCountryNamesByName.json");
                } else {

                    //oorsprong.org
                    $scope.profile.pais = data.data[194].sName;
                    // console.log(data.data[194].sName);
                }
            })
////////////////////////////////load province
    var data_provincia = {
        "load_data_provincia": true, }

    services.post('users', 'load_provincias_users', data_provincia)
            .then(function (data) {

                //console.log(data.data)

                if (data.data === 'error') {
                    //  load_countries_v2("app_model/resources/ListOfCountryNamesByName.json");
                } else {
                    $scope.provincia = data.data.provincias

                    //oorsprong.org
                    //$scope.profile.pais = data.data[194].sName;
                    //console.log(data.data[194].sName);
                }
            })

    ///////////////////////////load city
    $scope.selectCiudad = function () {
        // console.log($scope.profile.provincia);

        var datos = {nombreProvincia: $scope.profile.provincia};

        services.post('users', 'load_poblaciones_users', datos)
                .then(function (data) {

                    //  console.log(data.data)

                    if (data.data === 'error') {
                        //  load_countries_v2("app_model/resources/ListOfCountryNamesByName.json");
                    } else {
                        $scope.ciudades = data.data.poblaciones

                        //oorsprong.org
                        //$scope.profile.pais = data.data[194].sName;
                        //console.log(data.data[194].sName);
                    }
                })
    }



    $scope.update = function () {
        //////////////////////////////update user
        if ($scope.profile.provincia === '') {


            $scope.profile.ciudad = user.poblacion;
            $scope.profile.provincia = user.provincia;
        } else {
            $scope.profile.provincia;

            $scope.profile.ciudad;

        }

        if ($scope.profile.email !== user.email) {


            $scope.profile.email;

        } else {
            $scope.profile.email = user.email;


        }


        if ($scope.profile.password_old === "") {


            $scope.profile.password = user.password;

        } else {
            $scope.profile.password;


        }

        if ($scope.profile.deportes[0].checked === true) {
            $scope.profile.todos = 1;
        }
        console.log($scope.profile.radii[0]);
        console.log($scope.profile.deportes);
        //alert( nivel);
        var data = {
            "nombre": $scope.profile.nombre,
            "pais": $scope.profile.pais,
            "provincia": $scope.profile.provincia,
            "poblacion": $scope.profile.ciudad,
            "masculino": $scope.profile.radii[0].checked,
            "femenino": $scope.profile.radii[1].checked,
            "telefono": $scope.profile.telefono,
            "email": $scope.profile.email,
            "bajo": $scope.profile.nivel[0].checked,
            "medio": $scope.profile.nivel[1].checked,
            "alto": $scope.profile.nivel[2].checked,
            "password": $scope.profile.password,
            "fecha_nac": $scope.profile.fecha_nac,
            "fecha_registro": $scope.profile.fecha_registro,
            "password_old": $scope.profile.password_old,
            "status": user.status,
            "token": user.token,
            "tipo": user.tipo,
            "todos": $scope.profile.todos,
        }
        var datos = JSON.stringify(data)
        console.log(data);
        services.put('users', 'update_users', datos)
                .then(function (data) {
                    console.log(data)
                    if (data.data.success) {
                        console.log(data.data);
                        /*$scope.login.msg = data.data.message
                         
                         var now = new Date(),
                         // this will set the expiration to 1 day
                         exp = new Date(now.getFullYear(), now.getMonth(), now.getDate()+1);
                         $cookies.putObject('user', data.data.users, {
                         expires: exp
                         });
                         $timeout(function () {
                         $location.path('/');
                         }, 6000);*/

                    } else {

                        /*  $scope.login.fecha = data.data.error.fecha_nac;
                         $scope.login.msg = data.data.error.message;
                         
                         $scope.login.nombre_error = data.data.error.nombre;
                         $scope.login.email_error = data.data.error.email;*/
                        console.log(data.data.error);
                        console.log(data.data.success);
                    }

                });

    }
});



