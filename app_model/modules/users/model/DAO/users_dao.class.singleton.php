<?php

class users_dao {

    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function update_user_DAO($db, $arrArgument) {


        $nombre = $arrArgument['nombre'];

        $pais = $arrArgument['pais'];

        $poblacion = $arrArgument['poblacion'];
        $provincia = $arrArgument['provincia'];

        $telefono = $arrArgument['telefono'];
        $deportes = $arrArgument['deportes'];

        $deportes1 = 0;
        $deportes2 = 0;
        $deportes3 = 0;
        $deportes4 = 0;
        $deportes5 = 0;
        $deportes6 = 0;

        foreach ($deportes as $indice) {

            if ($indice == "Todos") {

                $deportes1 = 1;
            }
            if ($indice === "futbol") {
                $deportes2 = 1;
            }
            if ($indice === "baloncesto") {
                $deportes3 = 1;
            }
            if ($indice === "voleibol") {
                $deportes4 = 1;
            }
            if ($indice === "tenis") {
                $deportes5 = 1;
            }
            if ($indice === "padel") {
                $deportes6 = 1;
            }
        }
        $sexo = $arrArgument['sexo'];
        $email = $arrArgument['email'];

        $password = $arrArgument['password'];
        $fecha_nac = $arrArgument['fecha_nac'];

        $avatar = $arrArgument['avatar'];
        $nivel = $arrArgument['nivel'];




        $sql = "UPDATE users SET nombre='$nombre',apellidos='$sexo',numero='$telefono',poblacion='$poblacion',provincia='$provincia',todos='$deportes1',futbol='$deportes2',baloncesto='$deportes3',voleibol='$deportes4',tenis='$deportes5',padel='$deportes6',"
                . "nivel='$nivel',email='$email',password='$password', fecha_nac='$fecha_nac', avatar='$avatar' , pais='$pais'  WHERE nombre='$nombre'";


        return $db->ejecutar($sql);
    }

    public function obtain_paises_DAO($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $file_contents = curl_exec($ch);
        curl_close($ch);

        return ($file_contents) ? $file_contents : FALSE;
    }

    public function obtain_provincias_DAO() {
        $json = array();
        $tmp = array();

        $provincias = simplexml_load_file(RESOURCES . "provinciasypoblaciones.xml");
        $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
        for ($i = 0; $i < count($result); $i+=2) {
            $e = $i + 1;
            $provincia = $result[$e];

            $tmp = array(
                'id' => (string) $result[$i], 'nombre' => (string) $provincia
            );
            array_push($json, $tmp);
        }
        return $json;
    }

    public function obtain_poblaciones_DAO($arrArgument) {
        $json = array();
        $tmp = array();

        $filter = (string) $arrArgument;
        $xml = simplexml_load_file(RESOURCES . "provinciasypoblaciones.xml");
        $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");

        for ($i = 0; $i < count($result[0]); $i++) {
            $tmp = array(
                'poblacion' => (string) $result[0]->localidad[$i]
            );
            array_push($json, $tmp);
        }
        return $json;
    }

    public function activateUser_DAO($db, $token) {

        $sql = "SELECT * FROM users WHERE token='$token'";

        $stmt = $db->ejecutar($sql);


        if ($result = $db->obtener_fila($stmt, 0)) {

            $sql = "UPDATE users SET status='1'  WHERE nombre='" . $result['nombre'] . "'";
            $stmt = $db->ejecutar($sql);
        };
        return $result;
    }

    public function obtain_coord_DAO($db) {


        $sql = "SELECT * FROM game JOIN installation ON game.id_install=installation.id";
        
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }
    
    public function obtain_users_DAO($db) {


        $sql = "SELECT * FROM users";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }


    public function obtain_coord_near_DAO($db, $arrArgument) {


        $lat = $arrArgument['latitud'];
        $long = $arrArgument['longitud'];
        $start = $arrArgument['start'];
        $records_per_page = $arrArgument['records_per_page'];
        $minLat = $lat - 1;
        $maxLat = $lat + 1;

        $minLon = $long - 1;
        $maxLon = $long + 1;

        $sql = "SELECT * FROM game JOIN installation ON game.id_install=installation.id WHERE (installation.latitud between $minLat and $maxLat) and (installation.longitud between $minLon and $maxLon)LIMIT $start, $records_per_page";
         
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }
 public function obtain_coord_near_map_DAO($db, $arrArgument) {


        $lat = $arrArgument['latitud'];
        $long = $arrArgument['longitud'];
        
        $minLat = $lat - 1;
        $maxLat = $lat + 1;

        $minLon = $long - 1;
        $maxLon = $long + 1;

        $sql = "SELECT * FROM game JOIN installation ON game.id_install=installation.id WHERE (installation.latitud between $minLat and $maxLat) and (installation.longitud between $minLon and $maxLon)";
         
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function obtain_user_DAO($db, $arrArgument) {


        $sql = "SELECT * FROM users WHERE token='$arrArgument'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }
 public function count_games_DAO($db) {

        $sql = "SELECT COUNT(*) as total FROM game";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }
  public function exist_users_email_DAO($db, $arrArgument) {
        $nombre = $arrArgument['nombre'];
        $email = $arrArgument['email'];
    

        $sql = "SELECT COUNT(*) AS total_email FROM users WHERE email = '$email' and nombre='$nombre' ";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

}
