<?php

class controller_users {

    public function __construct() {

        include(FUNCTIONS_USERS . "functions.inc.php");
        include(UTILS . "upload.inc.php");

        $_SESSION['module'] = "users";
    }

    /////////////////////////////////////////////////// activar usuario

    public function activarUser() {

        //loadView('modules/users/view/', 'users.php');


        if (isset($_GET['aux'])) {


            $token = substr($_GET['aux'], 1); //extrae el token, coja la cadena a partir de la 2 posicion
            $controll = substr($_GET['aux'], 0, 1); //extrae la A para el control si es de activar o de recordar 



            if ($controll == 'A') {
                set_error_handler('ErrorHandler');
                try {

                    $arrValue = loadModel(MODEL_USERS, "users_model", "activateUser", $token);
                } catch (Exception $e) {
                    $arrValue = false;
                }
                restore_error_handler();

                if ($arrValue) {

                    $_SESSION['users'] = $arrValue;

                    $_SESSION['name'] = $arrValue['nombre'];
                    $_SESSION['token'] = $arrValue['token'];
                } else {
                    redirect("http://51.254.97.198/NFU/");
                }
                redirect("http://51.254.97.198/NFU/#/users");
            } elseif ($controll == 'L') {
                if ($_SESSION['users']['tipo'] === "admin") {
                    redirect("http://51.254.97.198/NFU/#/admin");
                } else {
                    redirect("http://51.254.97.198/NFU/#/users");
                }
            } elseif ($controll == 'R') {
                get_user($token);

                redirect("http://51.254.97.198/NFU/#/recovery_pass");
            } else {
                redirect("http://51.254.97.198/NFU/");
            }
        }
    }

    /////////////////////////////////////////////////// upload_image

    public function upload_users() {


        echo json_encode($_POST);
        die();
        if (isset($_POST) && ($_POST)) {

            $result_avatar = upload_files();

            $_SESSION['image'] = $result_avatar;
        }
    }

    /////////////////////////////////////////////////// update_usuario

    public function update_users() {



        $jsondata = array();

        //$usersJSON = json_decode($_POST["update_users_json"], true);
        $result = validate_users_update($_POST);


        //$result = validate_users_update($_POST);

        /* if (($_SESSION['image']['datos']) === $_SESSION['users']['avatar']) {
          $_SESSION['image'] = array('resultado' => true, 'error' => "", 'datos' => $_SESSION['users']['avatar']);
          } else if ($_SESSION['image'] == null) {
          $_SESSION['image'] = array('resultado' => true, 'error' => "", 'datos' => $_SESSION['users']['avatar']);
          } else {
          $_SESSION['image'] = array('resultado' => true, 'error' => "", 'datos' => MEDIA_PATH . $_SESSION['image']['datos']);
          }
         */

        $result_avatar = $_SESSION['image'];





        if ($_POST['email']) {

            $arrArgument = array(
                'nombre' => $_POST['nombre'],
                'email' => ($_POST['email']),
            );


            set_error_handler('ErrorHandler');
            try {

                $exist = loadModel(MODEL_USERS, "users_model", "exist_users_email", $arrArgument);
            } catch (Exception $e) {
                $jsondata["success"] = false;
                $jsondata["type_error"] = "503";
                echo json_encode($jsondata);
                exit;
            }


            restore_error_handler();
            if ($exist[0]['total_email'] === '0') {

                $resultado = array(
                    'email' => $usersJSON['email'],
                );
                $error = array(
                    'email' => "El email ya está registrado",
                );

                $jsondata['error'] = $error;
                $jsondata["success"] = false;

                echo json_encode($jsondata);
                exit;
            }
        }
//encripto la contraseña en el DAO porque allí realizao una comprobación
        if (($result['resultado'])) {//&& $result_avatar['resultado']
            $arrArgument = array(
                'nombre' => $_POST['nombre'],
                'pais' => ($_POST['pais']),
                'provincia' => ($_POST['provincia']),
                'poblacion' => ($_POST['poblacion']),
                'deportes' => ($_POST['deportes']),
                'sexo' => ($_POST['sexo']),
                'email' => ($_POST['email']),
                'password' => $_POST['password'],
                'fecha_nac' => ($_POST['fecha_nac']),
                'telefono' => $_POST['telefono'],
                'nivel' => $_POST['nivel'],
                //'avatar' => $result_avatar['datos'],
                'tipo' => $_POST['tipo'],
                'status' => $_POST['status'],
                'token' => $_POST['token'],
            );




            //$_SESSION['result_avatar'] = array();

            $_SESSION['avatar'] = array();
            ///////////////// update into BD begin ////////////////////////


            $arrValue = false;
            // $path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/users/model/model/';

            set_error_handler('ErrorHandler');
            try {

                $arrValue = loadModel(MODEL_USERS, "users_model", "update_user", $arrArgument);
            } catch (Exception $e) {
                $jsondata["success"] = false;
                $jsondata["type_error"] = "503";
                echo json_encode($jsondata);
                exit;
            }
            restore_error_handler();

            if ($arrValue) {

                //$_SESSION['users'] = $arrArgument;

                $_SESSION['msje'] = "Actualización correcta de sus datos";
                //$callback = SITE_PATH."index.php?module=users&function=users";
                $jsondata["success"] = true;

                echo json_encode($jsondata);
                exit;
            }



            //En el caso de enviar al usuario un correo y finalizar la aplicaci�n
            //include "results_user.php";
            //die;
        } else {
            //	$error = $result['error'];
            //debug($error);



            $jsondata["success"] = false;
            $jsondata["error"] = $result['error'];

            $jsondata["error_avatar"] = $result_avatar['error'];
            $jsondata["error_avatar"] = $result_avatar['error'];

            $jsondata["success1"] = false;
            if ($result_avatar['resultado']) {
                $jsondata["success1"] = true;
                $jsondata["img_avatar"] = $result_avatar['datos'];
            }


            //header('HTTP/1.0 404 Bad error', true, 404);
            echo json_encode($jsondata);
            //debugPHP($error);
        }
    }

////////////////////////////////////////////////////////count_games
    public function count_games() {

        $records_per_page = 3;
        set_error_handler('ErrorHandler');
        try {

            $resultado = loadModel(MODEL_USERS, "users_model", "count_games");

            $total_rows = $resultado;

            $count = $total_rows[0]["total"];

            $total_pages = ceil($count / $records_per_page);


            //throw new Exception(); //que entre en el catch
        } catch (Exception $e) {
            //showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            echo json_encode('error_DB');
            exit;
        }
        restore_error_handler();

        if ($total_pages) {

            echo json_encode($total_pages);
            exit;
        } else {
            //showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            echo json_encode('error_no_games');
            exit;
        }
    }

///////////////////////////////////////////////////error
    public function view_error_true() {
        if ((isset($_GET["view_error"])) && ($_GET["view_error"] == "true")) {
            //showErrorPage(0, "ERROR - 503 BD Unavailable");
            echo json_encode('error_DB');
            exit;
        }
    }

////////////////////////////////////////////////////error
    public function view_error_false() {
        if ((isset($_GET["view_error"])) && ($_GET["view_error"] == "false" )) {
            //showErrorPage(3, "RESULTS NOT FOUND");
            echo json_encode('error_no_games');
            exit;
        }
    }

    ///////////////////////////////////////loal_all_games

    public function load_coordinates() {


        if (isset($_POST["load_coord"]) && $_POST["load_coord"] == true) {

            set_error_handler('ErrorHandler');

            try {
//loadmodel
//$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/installation/model/model/';
                $results = loadModel(MODEL_USERS, "users_model", "obtain_coord");


//throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($results) {
                $jsondata["game"] = $results;
                echo json_encode($jsondata);
                exit;
            } else {
//if($nom_installationos){ //que lance error si no hay installationos
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    ///////////////////////////////////////loal_all_games_item_page

    public function load_coordinates_near() {

        if (isset($_POST["coord1"])) {

            $page = intval($_POST["coord1"]);
            $usersJSON = json_decode($_POST["coord1"], true);
            $page = intval($usersJSON['p']);
            set_error_handler('ErrorHandler');



            $current_page = $page - 1;
            $records_per_page = 3; // records to show per page
            $start = $current_page * $records_per_page;

            $arrargument = array(
                'start' => $start,
                'records_per_page' => $records_per_page,
                'latitud' => $usersJSON['lat_position'],
                'logitud' => $usersJSON['long_position'],
            );

            try {
//loadmodel
//$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/installation/model/model/';
                $results = loadModel(MODEL_USERS, "users_model", "obtain_coord_near", $arrargument);


//throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                $results = false;
            }
            restore_error_handler();

            if ($results) {


                $jsondata["game"] = $results;

                $jsondata["success"] = true;
                echo json_encode($jsondata);
                exit;
            } else {
//if($nom_installationos){ //que lance error si no hay installationos
                $jsondata["success"] = false;
                $jsondata["msje"] = "Error en la consulta partidas";
                exit;
            }
        } else {
            $jsondata["success"] = false;
            $jsondata["msje"] = "Error en la consulta partidas";
            exit;
        }
    }

    ///////////////////////////////////////loal_all_games_map

    public function load_coordinates_near_map() {
        if (isset($_POST["coord"])) {


            $usersJSON = json_decode($_POST["coord"], true);

            set_error_handler('ErrorHandler');


            $arrargument = array(
                'latitud' => $usersJSON['lat_position'],
                'logitud' => $usersJSON['long_position'],
            );

            try {
//loadmodel
//$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/installation/model/model/';
                $results = loadModel(MODEL_USERS, "users_model", "obtain_coord_near_map", $arrargument);


//throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                $results = false;
            }
            restore_error_handler();

            if ($results) {


                $jsondata["game"] = $results;

                $jsondata["success"] = true;
                echo json_encode($jsondata);
                exit;
            } else {
//if($nom_installationos){ //que lance error si no hay installationos
                $jsondata["success"] = false;
                $jsondata["msje"] = "Error en la consulta partidas";
                exit;
            }
        } else {
            $jsondata["success"] = false;
            $jsondata["msje"] = "Error en la consulta partidas";
            exit;
        }
    }

    /////////////////////////////////////////delete_upload_img
    public function delete_users() {
        if (isset($_POST["delete"]) && $_POST["delete"] == true) {

            $_SESSION['result_avatar'] = array();

            $result = remove_files();
            if ($result === true) {
                echo json_encode(array("res" => true));
            } else {
                echo json_encode(array("res" => false));
            }
        }
    }

    /////////////////////////////////////////////////// load_data_user
    public function load_data_users() {

        if (($_POST)) {


            $jsondata = array();
            set_error_handler('ErrorHandler');

            try {
                //loadmodel
                //$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/installation/model/model/';
                $results = loadModel(MODEL_USERS, "users_model", "obtain_user", $_POST);


                //throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                $jsondata["success"] = false;
                $jsondata["type_error"] = "503";
                echo json_encode($jsondata);
                exit;
                //showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if (isset($results)) {
                //$_SESSION['users'] = $results[0];

                $jsondata["users"] = $results[0];
                $jsondata["success"] = true;

                //$_SESSION['users']['avatar'] = $_SESSION['img'];
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["success"] = false;
                $jsondata["users"] = "";
                echo json_encode($jsondata);
                exit;
            }
        } else {
            $jsondata["success"] = false;
            $jsondata["users"] = "";
            echo json_encode($jsondata);
            exit;
        }
        //}
    }

/////////////////////////////////////////////////////////////////load all users admin
    public function obtain_users() {

        if ((isset($_POST["load_data_users"])) && ($_POST["load_data_users"] == true)) {


            $jsondata = array();
            set_error_handler('ErrorHandler');

            try {
                //loadmodel
                //$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/installation/model/model/';
                $results = loadModel(MODEL_USERS, "users_model", "obtain_users");


                //throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if (isset($results)) {
                //$_SESSION['users'] = $results[0];

                $jsondata["users"] = $results;

                $jsondata["success"] = true;

                //$_SESSION['users']['avatar'] = $_SESSION['img'];
                echo json_encode($jsondata);
                exit;
            } else {

                $jsondata["success"] = false;
                $jsondata["users"] = "";
                $jsondata["error"] = "No se han encontrado usuarios";
                echo json_encode($jsondata);
                exit;
            }
        } else {

            $jsondata["success"] = false;
            $jsondata["users"] = "";
            $jsondata["error"] = "No se han encontrado usuarios";
            echo json_encode($jsondata);
            exit;
        }
        //}
    }

    /////////////////////////////////////////////////// load_pais

    public function load_pais_users() {

        if ((isset($_POST)) && ($_POST == true)) {

            //  echo($_POST["load_pais"]);

            $json = array();

            $url = 'http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/ListOfCountryNamesByName/JSON';

            //$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/users/model/model/';

            set_error_handler('ErrorHandler');
            try {

                $json = loadModel(MODEL_USERS, "users_model", "obtain_paises", $url);
            } catch (Exception $e) {
                $json = array();
            }
            restore_error_handler();
            if ($json) {
                echo $json;
                exit;
            } else {
                $json = "error";
                echo $json;
                exit;
            }
        }
    }

    /////////////////////////////////////////////////// load_provincias
    public function load_provincias_users() {

        if (isset($_POST) && ($_POST == true)) {
            $jsondata = array();
            $json = array();

            // $path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/users/model/model/';
            set_error_handler('ErrorHandler');
            try {
                $json = loadModel(MODEL_USERS, "users_model", "obtain_provincias");
            } catch (Exception $e) {
                $json = array();
            }
            restore_error_handler();
            if ($json) {
                $jsondata["provincias"] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["provincias"] = "error";
                echo json_encode($jsondata);
                exit;
            }
        }
    }

    /////////////////////////////////////////////////// load_poblaciones
    public function load_poblaciones_users() {

        if (isset($_POST)) {
            $jsondata = array();
            $json = array();

            // $path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/users/model/model/';
            set_error_handler('ErrorHandler');
            try {
                $json = loadModel(MODEL_USERS, "users_model", "obtain_poblaciones", $_POST["nombreProvincia"]['id']);
            } catch (Exception $e) {
                $json = array();
            }
            restore_error_handler();
            if ($json) {
                $jsondata["poblaciones"] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["poblaciones"] = "error";
                echo json_encode($jsondata);
                exit;
            }
        }
    }

    /////////////************enviar email********* */////////

    /*  public function process_contact() {
      if($_POST['token'] === "formulario"){

      //////////////// Envio del correo al usuario
      $arrArgument = array(
      'type' => 'alta',
      'token' => '',
      'inputName' => $_POST['nombre'],
      'inputEmail' => $_POST['email'],

      );

      set_error_handler('ErrorHandler');
      try{
      if (enviar_email($arrArgument)){
      //echo($arrArgument);

      echo("<div class='alert alert-success'>Your message has been sent </div>") ;

      } else {
      echo ("<div class='alert alert-error'>Server error. Try later...</div>");

      }
      } catch (Exception $e) {
      echo "<div class='alert alert-error'>Server error. Try later...</div>";
      }
      restore_error_handler();


      //////////////// Envio del correo al admin de la ap web
      $arrArgument = array(
      'type' => 'admin',
      'token' => '',
      'inputName' => $_POST['nombre'],
      'inputEmail' => $_POST['email'],

      );
      set_error_handler('ErrorHandler');
      try{
      if (enviar_email($arrArgument)){

      //  echo "<div class='alert alert-success'>Your message has been sent </div>";
      } else {
      echo "<div class='alert alert-error'>Server error. Try later...</div>";
      }
      } catch (Exception $e) {
      echo "<div class='alert alert-error'>Server error. Try later...</div>";
      }
      restore_error_handler();

      }else{
      echo "<div class='alert alert-error'>Server error. Try later...</div>";
      }
      }


     */
}
