$(document).ready(function () {
    $('#form_hidden').hide();
    $("#progress").hide();


    $('#show_password').click(function () {
        //alert("hola");
        $('#form_hidden').toggle("slow");
    });

    var tel = /^(6|7)[0-9]{8}$/;
    var passwordre = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;

    /////////////////////////////////////////////////// cargar_datos_user
    load_user();

    /////////////////////////////////////////////////// datepicker

    $("#fecha_nac").datepicker({
        changeMonth: true,
        changeYear: true
    });

//realizamos funciones para que sea más práctico nuestro formulario
    $("#password, #password2, #telefono").keyup(function () {
        if ($(this).val() != "") {
            $(".error").fadeOut();
            return false;
        }
    });




    $("#password").keyup(function () {
        if (passwordre.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });


    $("#telefono").keyup(function () {
        if (tel.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });
    $("#password2").keyup(function () {
        if (passwordre.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $('#boton').click(function () {
        // alert("hola");
        //  validate_users();

        validate_update();





    });

    /////////////////////////////////////////////////// pluguin dropzone cargar imagen
    $("#dropzone").autoDiscover = false;
    $("#dropzone").dropzone({
        url: "/users/upload_users",
        params: {'upload': true},
        addRemoveLinks: true,
        maxFileSize: 1000,
        dictResponseError: "Ha ocurrido un error en el server",
        acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
        init: function () {
            this.on("success", function (file, response) {
                //alert(file);
                //alert(response);
                // console.log(response);
                $("#progress").show();
                $("#bar").width('100%');
                $("#percent").html('100%');
                $('.msg').text('').removeClass('msg_error');
                $('#e_avatar').text('');
                $('.msg').text('Success Upload!!').addClass('msg_ok')//.animate({'right': '300px'}, 150);
            });
        },
        complete: function (file) {
            // alert(file.status)
            if (file.status == "success") {
                //  alert("El archivo se ha subido correctamente: " + file.name);
            }
        },
        error: function (file) {
            // alert("Error subiendo el archivo " + file.name);
        },
        removedfile: function (file, serverFileName) {
            var name = file.name;
            // alert(name);
            $.ajax({
                type: "POST",
                url: "/users/delete_users",
                data: {'delete': true},
                ruta: "filename=" + name,
                success: function (ruta) {
                    //alert(ruta);
                    $("#progress").hide();
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    $("#e_avatar").html("");

                    var json = JSON.parse(ruta);
                    if (json.res === true) {
                        var element;
                        if ((element = file.previewElement) != null) {
                            element.parentNode.removeChild(file.previewElement);
                            //alert("Imagen eliminada: " + name);
                        } else {
                            false;
                        }
                    } else { //json.res == false, elimino la imagen también
                        var element;
                        if ((element = file.previewElement) != null) {
                            element.parentNode.removeChild(file.previewElement);
                        } else {
                            false;
                        }
                    }
                }
            });
        }
    });


    /* PROVINCIA, PAIS, POBLACION*/
    /////////////////////////////////////////////////// cargar provincia, pais, población

    load_countries_v1();

    $("#provincia").append('<option value="" selected="selected">Selecciona una Provincia</option>');

    $("#poblacion").append('<option value="" selected="selected">Selecciona una Poblacion</option>');


    load_provincias_v1();


    $("#provincia").change(function () {
        var prov = $(this).val();
        if (prov > 0) {
            load_poblaciones_v1(prov);
        } else {
            $("#poblacion").prop('disabled', false);
        }
    });
});


function validate_update() {
    var result = true;
    var tel = /^(6|7)[0-9]{8}$/;
    var passwordre = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    result = true;
    var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    $(".error").remove();
    if ($("#password_old").val() === "" && $("#password").val() === "" && $("#password2").val() === "") {
        result = true;
    }
    else if (!passwordre.test($("#password_old").val())) {

        $("#password_old").focus().after("<span class='error'>Password debe tener al menos 6 caracteres,una letra mayúscula, un carácter numerico</span>");
        result = false;
    }

    else if (!passwordre.test($("#password").val())) {


        $("#password").focus().after("<span class='error'>Password debe tener al menos 6 caracteres,una letra mayúscula, un carácter numerico</span>");
        result = false;
    } else if (!passwordre.test($("#password2").val()) || $("#password").val() !== $("#password2").val()) {

        $("#password2").focus().after("<span id='errores' class='error'>Los 2 password deben coincidir</span>");
        result = false;

    }
    if ($("#email").val() == "" || !emailreg.test($("#email").val()) || $("#email").val() == "Introduzca su email") {
        $("#email").focus().after("<span class='error'>Ingrese un email correcto</span>");
        result = false;
    }
    else if ($("#telefono").val() === "") {

        result = true;
    }
    else if (!tel.test($("#telefono").val())) {
        $("#telefono").focus().after("<span class='error'>Ingrese un telefono correcto</span>");
        result = false;
    }


    if (result === true) {
        var nombre = $("#nombre").val();
        var password_old = $("#password_old").val();
        var poblacion = $("#poblacion").val();
        var provincia = $("#provincia").val();
        var pais = $("#pais").val();
        var telefono = $("#telefono").val();
        var email = $("#email").val();
        var deportes = [];
        //si no funcion var deportes=new Array[];
        var j = 0;
        var gustos = document.getElementsByClassName('deportes');

        for (var i = 0; i < gustos.length; ++i) {
            if (gustos[i].checked) {
                deportes[j] = gustos[i].value;
                j++;

            }
        }


        //var sexo = $('input[name=sexo]:checked', '#formulario').val();
        var sexo = $('input[name=sexo]:checked', '#formulario').val();
        //alert(sexo);
        var nivel = $('input[name=nivel]:checked', '#formulario').val();
        //alert(nivel);
        var fecha_nac = document.getElementById('fecha_nac').value;


        if (pais == null) {
            provincia = 'default_pais';
        } else if (pais.length == 0) {
            pais = 'default_pais';
        } else if (pais === 'Selecciona una Pais') {
            return '';
        }
        if (provincia == null) {
            provincia = 'default_provincia';
        } else if (provincia.length == 0) {
            provincia = 'default_provincia';
        } else if (provincia === 'Selecciona una Provincia') {
            return '';
        }

        if (poblacion == null) {
            poblacion = 'default_poblacion';
        } else if (poblacion.length == 0) {
            poblacion = 'default_poblacion';
        } else if (poblacion === 'Selecciona una Poblacion') {
            return '';
        }
        var data = {
            "nombre": nombre,
            "pais": pais,
            "provincia": provincia,
            "poblacion": poblacion,
            "deportes": deportes,
            "sexo": sexo,
            "telefono": telefono,
            "email": email,
            "nivel": nivel,
            "password": $("#password").val(),
            "fecha_nac": fecha_nac,
            "password_old": password_old,
        }

        var data_users_JSON = JSON.stringify(data);

        //alert(data_users_JSON);


        $.post("/users/update_users", {update_users_json: data_users_JSON},
        function (response) {

            console.log(response);
            if (response.success) {
                //alert("actualizado");
                // console.log(response);

                swal("Su perfil se ha actualizado correctamente");
                // console.log(response);
                setTimeout(function () {
                    window.location.href = response.redirect;
                }, 3000);
            } else {
                $("#password_old").focus().after("<span class='error'>" + response.error.password_old + "</span>");
                $("#email").focus().after("<span class='error'>" + response.error.email + "</span>");
            }

            ///// per a debuguejar loadmodel ////////
            //alert(response);  
            //.log(response);
            //////////////////////////////////////////
            //}); //para debuguear
        }, "json").fail(function (xhr, textStatus, errorThrown) {
            //alert( "error" );

            // alert(xhr.status);
            //alert(textStatus);
            // alert(errorThrown);

            if (xhr.status === 0) {
                console.log('Not connect: Verify Network.');
            } else if (xhr.status == 404) {
                console.log('Requested page not found [404]');
            } else if (xhr.status == 500) {
                console.log('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                console.log('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                console.log('Time out error.');
            } else if (textStatus === 'abort') {
                console.log('Ajax request aborted.');
            } else {
                console.log('Uncaught Error: ' + xhr.responseText);
            }
            //console.log(xhr.responseText);
            xhr.responseJSON = JSON.parse(xhr.responseText);

            $("#e_avatar").html(xhr.responseJSON.error_avatar);

            if (!(xhr.responseJSON.success1)) {
                $("#progress").hide();
                $('.msg').text('').removeClass('msg_ok');
                $('.msg').text('Error Upload image!!').addClass('msg_error').animate({'right': '300px'}, 300);
            }


            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.nombre !== undefined && xhr.responseJSON.error.nombre !== null) {
                    //   $("#e_nombre").text(xhr.responseJSON.error.nombre);
                    $("#nombre").focus().after("<span class='error'>" + xhr.responseJSON.error.nombre + "</span>");
                }
            }


            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.poblacion !== undefined && xhr.responseJSON.error.poblacion !== null) {
                    // $("#e_date_birthday").text(xhr.responseJSON.error.fecha_nac);
                    $("#poblacion").focus().after("<span class='error'>" + xhr.responseJSON.error.poblacion + "</span>");

                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.pais !== undefined && xhr.responseJSON.error.pais !== null) {
                    // $("#e_date_birthday").text(xhr.responseJSON.error.fecha_nac);
                    $("#pais").focus().after("<span class='error'>" + xhr.responseJSON.error.pais + "</span>");

                }
            }

            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.provincia !== undefined && xhr.responseJSON.error.provincia !== null) {
                    // $("#e_date_birthday").text(xhr.responseJSON.error.fecha_nac);
                    $("#provincia").focus().after("<span class='error'>" + xhr.responseJSON.error.provincia + "</span>");

                }
            }

            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.deportes !== undefined && xhr.responseJSON.error.deportes !== null) {
                    // $("#e_date_birthday").text(xhr.responseJSON.error.fecha_nac);
                    $("#e_deportes").focus().after("<span class='error'>" + xhr.responseJSON.error.deportes + "</span>");

                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.email !== undefined && xhr.responseJSON.error.email !== null) {
                    //$("#e_email").text(xhr.responseJSON.error.email);
                    $("#email").focus().after("<span class='error'>" + xhr.responseJSON.error.email + "</span>");
                }
            }

            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.password_old !== undefined && xhr.responseJSON.error.password_old !== null) {
                    //$("#e_password2").text(xhr.responseJSON.error.password2);

                    $("#password_old").focus().after("<span class='error'>" + xhr.responseJSON.error.password_old + "</span>");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.password !== undefined && xhr.responseJSON.error.password !== null) {
                    //$("#e_password").text(xhr.responseJSON.error.password);
                    $("#password").focus().after("<span class='error'>" + xhr.responseJSON.error.password + "</span>");
                }
            }


            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.password2 !== undefined && xhr.responseJSON.error.password2 !== null) {
                    //$("#e_password2").text(xhr.responseJSON.error.password2);

                    $("#password2").focus().after("<span class='error'>" + xhr.responseJSON.error.password2 + "</span>");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.fecha_nac !== undefined && xhr.responseJSON.error.fecha_nac !== null) {
                    // $("#e_date_birthday").text(xhr.responseJSON.error.fecha_nac);
                    $("#fecha_nac").focus().after("<span class='error'>" + xhr.responseJSON.error.fecha_nac + "</span>");

                }
            }

        });
    }
}

function load_user() {
    $.post("/users/load_data_users", {load_data: true},
    function (response) {
        console.log(response);


        $("#nombre").val(response.users.nombre);

        $("#poblacion").val(response.users.poblacion);
        $("#provincia").attr("value", response.users.provincia);

        $("#pais").attr("value", response.users.pais);
        $('#sexo').attr("value", response.users.apellidos).attr("checked", true);

        if (response.users.todos === "1") {
            $("#todos").attr('checked', true);

        } else {
            $('#todos').attr('checked', false);
        }
        if (response.users.futbol === "1") {
            $("#futbol").attr('checked', true);

        } else {
            $('#futbol').attr('checked', false);
        }

        if (response.users.baloncesto === "1") {
            $("#basket").attr('checked', true);

        } else {
            $('#basket').attr('checked', false);
        }

        if (response.users.voleibol === "1") {
            $("#volei").attr('checked', true);

        } else {
            $('#volei').attr('checked', false);
        }

        if (response.users.padel === "1") {
            $("#padel").attr('checked', true);

        } else {
            $('#padel').attr('checked', false);
        }
        if (response.users.tenis === "1") {
            $("#tenis").attr('checked', true);

        } else {
            $('#tenis').attr('checked', false);
        }



        if (response.users.apellidos === "femenino") {
            $('#sexo2').attr('checked', true);
            $('#sexo1').attr('checked', false);
        } else {
            $('#sexo1').attr('checked', true);
            $('#sexo2').attr('checked', false);
        }

        if (response.users.nivel === "bajo") {
            $('#nivelB').attr('checked', true);
            $('#nivelM').attr('checked', false);
            $('#nivelA').attr('checked', false);
        } else if (response.users.nivel === "medio") {
            $('#nivelB').attr('checked', false);
            $('#nivelM').attr('checked', true);
            $('#nivelA').attr('checked', false);

        } else if (response.users.nivel === "alto") {
            $('#nivelA').attr('checked', true);
            $('#nivelB').attr('checked', false);
            $('#nivelM').attr('checked', false);
        }

        if (response.users.email === "") {
            $("#email").prop("disabled", false);
        } else {
            $("#email").prop("disabled", true);
        }
        $("#email").attr("value", response.users.email);
        $("#telefono").attr("value", response.users.numero);
        $("#fecha_nac").attr("value", response.users.fecha_nac);


    }, "json");
}

function validate_pais(pais) {
    if (pais == null) {
        //return 'default_pais';
        return false;
    }
    if (pais.length == 0) {
        //return 'default_pais';
        return false;
    }
    if (pais === 'Selecciona un Pais') {
        //return 'default_pais';
        return false;
    }
    if (pais.length > 0) {
        var regexp = /^[a-zA-Z]*$/;
        return regexp.test(pais);
    }
    return false;
}
function validate_provincia(provincia) {
    if (provincia == null) {
        return 'default_provincia';
    }
    if (provincia.length == 0) {
        return 'default_provincia';
    }
    if (provincia === 'Selecciona una Provincia') {
        //return 'default_provincia';
        return false;
    }
    if (provincia.length > 0) {
        var regexp = /^[a-zA-Z0-9, ]*$/;
        return regexp.test(provincia);
    }
    return false;
}
function validate_poblacion(poblacion) {
    if (poblacion == null) {
        return 'default_poblacion';
    }
    if (poblacion.length == 0) {
        return 'default_poblacion';
    }
    if (poblacion === 'Selecciona una Poblacion') {
        //return 'default_poblacion';
        return false;
    }
    if (poblacion.length > 0) {
        var regexp = /^[a-zA-Z/, -'()]*$/;
        return regexp.test(poblacion);
    }
    return false;
}





function load_countries_v2(cad, variable) {
    $.post(cad, variable, function (data) {

        var json = JSON.parse(data);

        for (var i = 0; i < json.length; i++) {
            if (json[i].sISOCode == "ES") {
                $("#pais").append("<option value='" + json[i].sISOCode + "'>" + json[i].sName + "</option>");
            }
        }


    })
            .fail(function () {
                alert("error load_countries");
            });
}

function load_countries_v1() {
    $.post("/users/load_pais_users/", {load_pais: true},
    function (response) {
        //console.log(response);
        if (response === 'error') {
            load_countries_v2("/resources/ListOfCountryNamesByName.json");
        } else {
            load_countries_v2("/users/load_pais_users/", {load_pais: true}); //oorsprong.org
        }
    })
            .fail(function (response) {
                load_countries_v2("resources/ListOfCountryNamesByName.json");
            });
}

function load_provincias_v2() {
    $.post("/resources/provinciasypoblaciones.xml", function (xml) {
        $("#provincia").empty();
        $("#provincia").append('<option value="" selected="selected">Selecciona una Provincia</option>');

        $(xml).find("provincia").each(function () {
            var id = $(this).attr('id');
            var nombre = $(this).find('nombre').text();
            $("#provincia").append("<option value='" + id + "'>" + nombre + "</option>");
        });
    })
            .fail(function () {
                alert("error load_provincias");
            });
}

function load_provincias_v1() { //provinciasypoblaciones.xml - xpath
    $.post("/users/load_provincias_users", {load_provincias: true},
    function (response) {
        $("#provincia").empty();
        $("#provincia").append('<option value="" selected="selected">Selecciona una Provincia</option>');

        //console.log(response);
        var json = JSON.parse(response);
        var provincias = json.provincias;
        //alert(provincias);
        //console.log(provincias);

        //alert(provincias[0].id);
        //alert(provincias[0].nombre);

        if (provincias === 'error') {
            load_provincias_v2();
        } else {
            for (var i = 0; i < provincias.length; i++) {
                $("#provincia").append("<option value='" + provincias[i].id + "'>" + provincias[i].nombre + "</option>");
            }
        }
    })
            .fail(function (response) {
                load_provincias_v2();
            });
}

function load_poblaciones_v2(prov) {
    $.post("/resources/provinciasypoblaciones.xml", function (xml) {
        $("#poblacion").empty();
        $("#poblacion").append('<option value="" selected="selected">Selecciona una Poblacion</option>');

        $(xml).find('provincia[id=' + prov + ']').each(function () {
            $(this).find('localidad').each(function () {
                $("#poblacion").append("<option value='" + $(this).text() + "'>" + $(this).text() + "</option>");
            });
        });
    })
            .fail(function () {
                alert("error load_poblaciones");
            });
}

function load_poblaciones_v1(prov) { //provinciasypoblaciones.xml - xpath
    var datos = {idPoblac: prov};
    $.post("/users/load_poblaciones_users", datos, function (response) {
        //alert(response);
        var json = JSON.parse(response);
        var poblaciones = json.poblaciones;
        //alert(poblaciones);
        //console.log(poblaciones);
        //alert(poblaciones[0].poblacion);

        $("#poblacion").empty();
        $("#poblacion").append('<option value="" selected="selected">Selecciona una Poblacion</option>');

        if (poblaciones === 'error') {
            load_poblaciones_v2(prov);
        } else {
            for (var i = 0; i < poblaciones.length; i++) {
                $("#poblacion").append("<option value='" + poblaciones[i].poblacion + "'>" + poblaciones[i].poblacion + "</option>");
            }
        }
    })
            .fail(function () {
                load_poblaciones_v2(prov);
            });
}











function paint(dataString) {
    $("#resultMessage").html(dataString).fadeIn("slow");

    setTimeout(function () {
        $("#resultMessage").fadeOut("slow")
    }, 5000);

    //reset the form
    $('#formulario')[0].reset();

    // hide ajax loader icon
    $('.ajaxLoader').fadeOut("fast");
    $("#nombre").val('');

    $("#password").val('');
    $("#password2").val('');
    $("#email").val('');
    $("#fecha_nac").val('');


    /*if (dataString == "<div class='alert alert-success'>Your message has been sent </div>") {
     //alert(dataString);
     } else {
     // alert(dataString);
     }*/
}