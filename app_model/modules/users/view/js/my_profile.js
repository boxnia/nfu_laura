$(document).ready(function () {
    //var geocoder;
    ////////////////reset
    $("#results").html("");

    current_page = 1;
    var loading = false;
    var oldscroll = 0;
    ///////////////////////////geolocalizar
    if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
        var watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, {timeout: 3000});
        google.maps.event.addDomListener(window, 'load', onSuccess);

    } else {
        document.addEventListener("deviceready", onDeviceReady, false);

        var watchID = null;


        function onDeviceReady() {

            navigator.geolocation.getCurrentPosition(onSuccess, onError, {enableHighAccuracy: false, timeout: 3000});

        }
    }

    function onSuccess(position) {

///////////////////////lat y long geolocalizado
        lat = position.coords.latitude;
        lang = position.coords.longitude;
        // console.log(lat);

        var myLatlng = new google.maps.LatLng(lat, lang);



        var mapOptions = {
            zoom: 14,
            center: myLatlng
        }

////////////////////////////mapa a cargar en map con la geolocaclizacion
        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lang),
            map: map,
            animation: google.maps.Animation.DROP
        });

        $.post("/users/count_games", function (data, status) {
            var json = JSON.parse(data);

            // total_pages == the number of scrolls that will need to see all rooms
            total_pages = json;
            // alert(total_pages);
        }).fail(function () {

            $("#results").load("/users/view_error_false", {'view_error': false});  //view_error=false
        });
        //initialize();
        //codeLatLng(lat, lang, myLatlng);
/////////////////////datos para calcular distancia al usuario de la partida
        var data = {
            "lat_position": lat,
            "long_position": lang,
            // "p": current_page,
        }

        var data_coord = JSON.stringify(data);


        //alert(data_coord);


        $.post("/users/load_coordinates_near_map",
                {coord: data_coord},
        function (data) {
            // console.log(response)

            // console.log(data);
            //console.log(response.succes);


            var game = data.game;
            console.log(game);

            for (i = 0; i < game.length; i++) {

                var first_point = new google.maps.LatLng(lat, lang);
                var second_point = new google.maps.LatLng(game[i].latitud, game[i].longitud);
                var distancia = google.maps.geometry.spherical.computeDistanceBetween(first_point, second_point);
                var marker, i;
                var icon = "";
                //console.log(distancia);

                if (distancia < 5000) {

                    //console.log(deporte);

                    if (game[i].deporte === 'tenis') {
                        icon = ruta_image("Tenis.svg");
                    } else if (game[i].deporte === 'futbol') {

                        icon = ruta_image("Futbol.svg");
                    }
                    else if (game[i].deporte === 'baloncesto') {

                        icon = ruta_image("Basket.svg");
                    }
                    else if (game[i].deporte === 'padel') {

                        icon = ruta_image("Padel.svg");
                    } else if (game[i].deporte === 'voleibol') {
                        icon = ruta_image("Volei.svg");
                    }


                    var lati = game[i].latitud;
                    var longi = game[i].longitud;



                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(lati, longi),
                        map: map,
                        icon: icon,
                        animation: google.maps.Animation.DROP
                    });


                }
            }

            var data1 = {
                "lat_position": lat,
                "long_position": lang,
                "p": current_page,
            }

            data_coord1 = JSON.stringify(data1);
//alert(data_coord1);

            $.post("/users/load_coordinates_near",
                    {coord1: data_coord1},
            function (data) {
                // console.log(response)

                // console.log(data);
                //console.log(response.succes);


                var game = data.game;
                console.log(game);




                for (j = 0; j < game.length; j++) {

                    if (game[j].deporte === 'tenis') {
                        icon = ruta_image("Tenis.svg");
                    } else if (game[j].deporte === 'futbol') {

                        icon = ruta_image("Futbol.svg");
                    }
                    else if (game[j].deporte === 'baloncesto') {

                        icon = ruta_image("Basket.svg");
                    }
                    else if (game[j].deporte === 'padel') {

                        icon = ruta_image("Padel.svg");
                    } else if (game[j].deporte === 'voleibol') {
                        icon = ruta_image("Volei.svg");
                    }

                    var games = ' <div class="row">\n\
                    <div class="col-md-3">\n\
                    <div class="hour"> ' + game[j].hora + '</div>\n\
                    <img class="nombre_producto" src="' + icon + '" alt="game">\n\
                    </div>\
                    <div class="col-md-6">\n\
                    <label> <strong>' + game[j].nombre + '</strong> </label>\n\
                    <p>' + game[j].descripcion + '</p>\n\
                    <h5> <strong>Idioma: ' + game[j].descripcion + ' </strong> </h5>\n\
                    <h5> <strong>Personas Conectadas: ' + game[j].descripcion + '</strong> </h5>\n\
                    </div>\n\
                    </div>\n\
                    </div>';


                    $(games).hide().appendTo('#results').fadeIn(1000);

//alert(current_page);
                   


                }
                 current_page++;
            }, "json")

        }, "json")
                .fail(function (xhr, textStatus, errorThrown) {

                });

        $(window).scroll(function () {
  //$('.ajaxLoader').fadeIn("slow");
            if ($(window).scrollTop() > oldscroll) {
                if (($(window).scrollTop() + $(window).height() >= $(document).height()) && (current_page <= total_pages)) {

//
                    //if (!loading) {
                    // loading = true;
                    // $('#results').addClass('loading');
                    var data2 = {
                        "lat_position": lat,
                        "long_position": lang,
                        "p": current_page,
                    }


                    data_coord1 = JSON.stringify(data2);
                    //alert(data_coord1);
                    $.post("/users/load_coordinates_near",
                            {coord1: data_coord1},
                    function (data) {
                          $('.ajaxLoader').fadeOut("fast");
                        // console.log(response)

                        console.log(data);
                        //console.log(response.succes);


                        var game = data.game;
                        console.log(game);




                        for (i = 0; i < game.length; i++) {

                            if (game[i].deporte === 'tenis') {
                                icon = ruta_image("Tenis.svg");
                            } else if (game[i].deporte === 'futbol') {

                                icon = ruta_image("Futbol.svg");
                            }
                            else if (game[i].deporte === 'baloncesto') {

                                icon = ruta_image("Basket.svg");
                            }
                            else if (game[i].deporte === 'padel') {

                                icon = ruta_image("Padel.svg");
                            } else if (game[i].deporte === 'voleibol') {
                                icon = ruta_image("Volei.svg");
                            }

                            var games = ' <div class="row">\n\
                    <div class="col-md-3">\n\
                    <div class="hour"> ' + game[i].hora + '</div>\n\
                    <img class="nombre_producto" src="' + icon + '" alt="game">\n\
                    </div>\
                    <div class="col-md-6">\n\
                    <label> <strong>' + game[i].nombre + '</strong> </label>\n\
                    <p>' + game[i].descripcion + '</p>\n\
                    <h5> <strong>Idioma: ' + game[i].descripcion + ' </strong> </h5>\n\
                    <h5> <strong>Personas Conectadas: ' + game[i].descripcion + '</strong> </h5>\n\
                    </div>\n\
                    </div>\n\
                    </div>';

                            $('#img_deporte').html('<img src="' + icon + '"> ');
                            //var cad12 = ruta_image("ubicacion.png");
                            $('#name_game').html(game[i].nombre);
                            //  $('#ubicacion').html("<a target='_blank' href='http://maps.google.com/?q= " + game[i].latitud + " ," + game[i].longitud + "'>" + '<img class="polaroids" height="52" width="42" src="' + cad12 + '"> ');
                            $('#hour').html(game[i].hora);


                            $(games).hide().appendTo('#results').fadeIn(1000);



                        }
                        // }

                    }, "json")


                    current_page++;
                    
                    


                    // }
                }
            }
        });



    }


$('.ajaxLoader').fadeOut("fast");

});
function onError(error) {
    //alert("hola");
    switch (error.code) {
        case error.PERMISSION_DENIED:
            alert("User denied the request for Geolocation.");
            break;
        case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable.")
            break;
        case error.TIMEOUT:
            alert("Tu gps está apagado!!")
            break;
        case error.UNKNOWN_ERROR:
            alert("An unknown error occurred.")
            break;
    }
}

/*  function initialize() {
 geocoder = new google.maps.Geocoder();
 
 
 
 }
 
 function codeLatLng(lat, lng, myLatlng) {
 
 var latlng = new google.maps.LatLng(lat, lng);
 geocoder.geocode({'latLng': myLatlng}, function (results, status) {
 if (status == google.maps.GeocoderStatus.OK) {
 //console.log(results);
 if (results[1]) {
 var indice = 0;
 for (var j = 0; j < results.length; j++)
 {
 if (results[j].types[0] == 'locality')
 {
 indice = j;
 break;
 }
 }
 //alert('The good number is: ' + j);
 // console.log(results[j]);
 for (var i = 0; i < results[j].address_components.length; i++)
 {
 if (results[j].address_components[i].types[0] == "locality") {
 //this is the object you are looking for
 city = results[j].address_components[i];
 console.log(city);
 }
 if (results[j].address_components[i].types[0] == "administrative_area_level_1") {
 //this is the object you are looking for
 region = results[j].address_components[i];
 }
 if (results[j].address_components[i].types[0] == "country") {
 //this is the object you are looking for
 country = results[j].address_components[i];
 }
 }
 
 //city data
 //alert(city.long_name + " || " + region.long_name + " || " + country.short_name)
 
 
 } else {
 alert("No results found");
 }
 //}
 } else {
 alert("Geocoder failed due to: " + status);
 }
 });
 }
 
 
 });*/



function ruta_image(image) {
    //alert(image);
    var project = "http://51.254.97.198/NFU/media/"
    var ruta = project + image;
    return ruta;
}