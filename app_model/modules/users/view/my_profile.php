<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>form-elements.css">
<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>style.css">
<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo USERS_CSS_PATH ?>styles.css">
<script type="text/javascript" src="<?php echo USERS_JS_PATH ?>my_profile.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo USERS_CSS_PATH ?>styles.css">


<div class="container">
    <div class="row">
        <div class="col-md-6 ">
            <div id="map">


            </div>
        </div>
        <div class="col-md-6" >
            <div class="bottom-buffer bottom-bordered top-buffer-40" style="padding-bottom:15px">
                <div class="form-inline right-buffer text-center">

                    <div class="form-group">
                        <div class="btn-group datesBlock" data-toggle="buttons">                                                                              
                            <div class="btn btn-default  dateSelector">                                            
                                <span>Hoy</span><input type="radio" name="dateoption" id="date1" value="1453475364270" autocomplete="off">
                            </div>
                            <div class="btn btn-default  dateSelector">                                            
                                <span>Mañana</span><input type="radio" name="dateoption" id="date2" value="1453561764270" autocomplete="off">
                            </div>

                        </div>
                    </div>                    
                </div>
                <div class="row">
                    <div class="col-md-6" >
                        <div class="form-inline right-buffer text-center top-buffer">
                            <div class="form-group left-buffer-info" >
                                <label for="filter" class="lead">Partidas </label>
                                <select class="form-control input-lg" name="filter" id="filter" style="max-width:220px">
                                    <option value="1">Todos las partidas</option>
                                    <option value="2" selected>De mis deportes</option>
                                    <!--<option value="3">De mis amigos y grupos</option>-->
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" >
                        <div class="form-group left-buffer-info" id="provincia-input">                        
                            <label for="provincia" class="lead">Provincia</label>
                            <select name="provincia" class="form-control input-lg" id="provincia" style="max-width:220px">
                                <option value='0'>Lugar</option>
                                <option value='15'                                                                        
                                        >A Coruña</option>
                                <option value='1'                                                                        
                                        >Álava</option>
                                <option value='2'                                                                        
                                        >Albacete</option>
                                <option value='3'                                                                        
                                        >Alicante</option>
                                <option value='4'                                                                        
                                        >Almería</option>
                                <option value='33'                                                                        
                                        >Asturias</option>
                                <option value='5'                                                                        
                                        >Ávila</option>
                                <option value='6'                                                                        
                                        >Badajoz</option>
                                <option value='7'                                                                        
                                        >Baleares (Illes)</option>
                                <option value='8'                                                                        
                                        >Barcelona</option>
                                <option value='9'                                                                        
                                        >Burgos</option>
                                <option value='10'                                                                        
                                        >Cáceres</option>
                                <option value='11'                                                                        
                                        >Cádiz</option>
                                <option value='39'                                                                        
                                        >Cantabria</option>
                                <option value='12'                                                                        
                                        >Castellón</option>
                                <option value='51'                                                                        
                                        >Ceuta</option>
                                <option value='13'                                                                        
                                        >Ciudad Real</option>
                                <option value='14'                                                                        
                                        >Córdoba</option>
                                <option value='16'                                                                        
                                        >Cuenca</option>
                                <option value='17'                                                                        
                                        >Girona</option>
                                <option value='18'                                                                        
                                        >Granada</option>
                                <option value='19'                                                                        
                                        >Guadalajara</option>
                                <option value='20'                                                                        
                                        >Guipúzcoa</option>
                                <option value='21'                                                                        
                                        >Huelva</option>
                                <option value='22'                                                                        
                                        >Huesca</option>
                                <option value='23'                                                                        
                                        >Jaén</option>
                                <option value='26'                                                                        
                                        >La Rioja</option>
                                <option value='35'                                                                        
                                        >Las Palmas</option>
                                <option value='24'                                                                        
                                        >León</option>
                                <option value='25'                                                                        
                                        >Lleida</option>
                                <option value='27'                                                                        
                                        >Lugo</option>
                                <option value='28'                                                                        
                                        >Madrid</option>
                                <option value='29'                                                                        
                                        >Málaga</option>
                                <option value='52'                                                                        
                                        >Melilla</option>
                                <option value='30'                                                                        
                                        >Murcia</option>
                                <option value='31'                                                                        
                                        >Navarra</option>
                                <option value='32'                                                                        
                                        >Ourense</option>
                                <option value='34'                                                                        
                                        >Palencia</option>
                                <option value='36'                                                                        
                                        >Pontevedra</option>
                                <option value='37'                                                                        
                                        >Salamanca</option>
                                <option value='38'                                                                        
                                        >Santa Cruz de Tenerife</option>
                                <option value='40'                                                                        
                                        >Segovia</option>
                                <option value='41'                                                                        
                                        >Sevilla</option>
                                <option value='42'                                                                        
                                        >Soria</option>
                                <option value='43'                                                                        
                                        >Tarragona</option>
                                <option value='44'                                                                        
                                        >Teruel</option>
                                <option value='45'                                                                        
                                        >Toledo</option>
                                <option value='46'                                                                        
                                        >Valencia</option>
                                <option value='47'                                                                        
                                        >Valladolid</option>
                                <option value='48'                                                                        
                                        >Vizcaya</option>
                                <option value='49'                                                                        
                                        >Zamora</option>
                                <option value='50'                                                                        
                                        >Zaragoza</option>
                            </select>
                        </div>
                    </div>

                    <div class="row" id="border">
                          <div class="form-group">
                        <div id="results"></div>
                                                        <img src="<?php echo USERS_IMG_PATH; ?>ajax-loader.gif" alt="ajax loader icon" class="ajaxLoader" />
                          </div>
                          </div>
                </div>

                <!--<div class="row" id="border">

                    <div class="col-md-3">
                        <div id="hour" ></div>
                        <div id="img_deporte" ></div>
                    </div>
                    <div class="col-md-6">
                        <center><strong><label id="name_game" class="details"></label></strong></center><br>
                        <center><strong><label id="ubicacion" class="details"></label></strong></center><br>


                    </div>
                    <div id="loader"></div>
                </div> -->        
            </div>
        </div>

    </div>

</div>
