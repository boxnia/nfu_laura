$(document).ready(function () {
        
    $('#boton_recovery_pass').click(function () {
        // alert("hola");
        recovery_pass();

    });
    
    
    $("#recovery_pass, #recovery_pass2").keyup(function () {
        if ($(this).val() != "") {
            $(".error").fadeOut();
            return false;
        }
    });
});
/////////////////////////////////////////confirm_password
function recovery_pass(){
    var result = true;    
    var passwordre = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    
    
    
    $(".error").remove();
    
    if ($("#recovery_pass").val() == "" || !passwordre.test($("#recovery_pass").val()) || $("#recovery_pass").val() == "Introduzca su password") {
        $("#recovery_pass").focus().after("<span class='error'>Password debe tener al menos 6 caracteres,una letra mayúscula, un carácter numerico</span>");
        result = false;
    } else if ($("#recovery_pass2").val() == "" || !passwordre.test($("#recovery_pass2").val()) || $("#recovery_pass2").val() == "Introduzca su password" || !$("#recovery_pass").val() == $("#recovery_pass2").val()) {
        $("#recovery_pass2").focus().after("<span class='error'>Los 2 password deben coincidir</span>");
        result = false;
    }else {

        result = true;
    }
    
    var password = $("#recovery_pass").val();
    var password1 = $("#recovery_pass2").val();
    
    
    if (result){
        var data = {            
            "password": password,
            "password2": password1,            
        }
        
        var data_pass_JSON = JSON.stringify(data);
        
       // alert(data_pass_JSON);
        $.post("/login/email_recovery_pass", {data_pass_JSON: data_pass_JSON},
        function (response) {
            console.log(response);
            if (!response.success) {
                /*alert(response.mensaje);
                /*$("#boton_recovery_pass").focus().after("<div class='error'>"+response.mensaje+"</div>");                
                swal(response.mensaje); 
                setTimeout(function(){ window.location.href = response.redirect; }, 3000);    */    
                $("#boton_recovery_pass").focus().after("<div class='error'><center>"+response.mensaje+"</center></div>");
            }else{
                /*alert(response.mensaje);*/
                /*$("#boton_recovery_pass").focus().after("<div class='error'>"+response.mensaje+"</div>");*/                
                swal(response.mensaje); 
                setTimeout(function(){ window.location.href = response.redirect; }, 3000);    
            }
        }, "json")/*.fail(function () {      
            console.log(response.mensaje);
            $("#boton_recovery_pass").focus().after("<div class='error'>"+response.mensaje+"</div>");
        });*/
        
        
        
    }
}



