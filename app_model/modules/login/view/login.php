<script type="text/javascript" src="<?php echo LOGIN_JS_PATH ?>login.js" ></script>
<script type="text/javascript" src="<?php echo LOGIN_JS_PATH ?>signin.js" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>form-elements.css">
<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>style.css">
<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo LOGIN_CSS_PATH ?>login.css">
 
<body>

    <!-- Top content -->
    <div class="top-content">


        <div class="container">



            <div class="row">
                <div class="col-sm-5">

                    <div class="form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Inicia sesión</h3>
                                <p></p>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-key"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <form  name="formulario_registro"  id="formulario-registro" class="registro">
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Usuario</label>
                                    <input type="text" name="nombre_singin" id="nombre_singin"  placeholder="Usuario..." class="form-username form-control">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="form-password">Password</label>
                                    <input type="password" name="password_singin" placeholder="Password..." class="form-password form-control" id="password_singin">
                                </div>

                                <input  id="boton_loguear" type="button"  name="boton_loguear" class="btn boton_loguear" value="Iniciar sesión" />
                                <label class="pull-right" id="recovery">Recuperar contraseña</label>
                            </form>
                        </div>
                    </div>

                    <div class="social-login">
                        <h3>...o inicia sesión con:</h3>
                        <div class="social-login-buttons">
                            <a class="btn btn-link-1 btn-link-1-facebook" href="#" id="facebook">
                                <i class="fa fa-facebook"></i> Facebook
                            </a>
                            <a class="btn btn-link-1 btn-link-1-twitter" id="twitter" href="#">
                                <i class="fa fa-twitter"></i> Twitter
                            </a>
                            <a class="btn btn-link-1 btn-link-1-google-plus" href="#">
                                <i class="fa fa-google-plus"></i> Google Plus
                            </a>
                        </div>
                    </div>

                </div>


                <div class="col-sm-1"></div>

                <div class="col-sm-5">

                    <div class="form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Registrate</h3>
                                <p></p>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-pencil"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <form name="formulario"  id="formulario" class="contacto">
                                <div class="form-group">
                                    <label class="sr-only" for="form-first-name">Usuario</label>
                                    <input type="text" name="nombre" placeholder="Usuario..." class=" nombre form-first-name form-control" id="nombre" required autofocus>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="form-password">Password</label>
                                    <input type="password" name="password" placeholder="Password..." class="password form-password form-control" id="password" required>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="form-password">Confirma Password</label>
                                    <input type="password" name="password2" placeholder="Confirmar password..." class="password2 form-password form-control" id="password2" required>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">Email</label>
                                    <input type="text" name="email" placeholder="Email..." class="email form-email form-control " id="email" required>
                                </div>

                                <div class="form-group">
                                    <label class="sr-only" for="form-about-yourself">Fecha Nacimiento</label>
                                    <div id="result"><input id="fecha_nac" type="text" name="fecha_nac" placeholder="mm/dd/yyyy"  id="fecha_nac"  required value="<?php echo $_POST ? $_POST['fecha_nac'] : ""; ?>" >
                                    </div> </div>



                                <input  id="boton" type="button"  name="boton" class="btn boton" value="Registrate!" />
                                <input type="hidden" name="token" value="formulario" />
                                <img src="<?php echo USERS_IMG_PATH; ?>ajax-loader.gif" alt="ajax loader icon" class="ajaxLoader" /><br/><br/>

                                <div id="resultMessage" style="display: none;"></div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>