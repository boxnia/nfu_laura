<!--<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">-->
<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>form-elements.css">

<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>style.css">  
<script type="text/javascript" src="<?php echo CONTACT_LIB_PATH; ?>jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo CONTACT_JS_PATH ?>contact.js" ></script>    

<body>

    <!-- Top content -->
    <div class="top-content">


        <div class="container">

            <div class="row">
                <div class="col-sm-5">
                    <div class="form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Contacto</h3>
                                <p>Rellena los campos y envianos un mensaje para mejorar tu experiencia:</p>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-envelope"></i>
                            </div>
                        </div>
                        <div class="form-bottom contact-form">                
                            <form id="form_contact" class="contact-form" name="form_contact">
                                <div class="form-group">
                                    <label class="sr-only" for="contact-nombre">Nombre</label>
                                    <input type="text" name="inputName" placeholder="Nombre..." class="contact-email form-control" id="inputName">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="contact-email">Email</label>
                                    <input type="email" name="inputEmail" placeholder="Email..." class="contact-email form-control" id="inputEmail">
                                </div>
                                <div class="form-group-contact">
                                    <select class="contact-subject form-control" id="inputSubject" name="inputSubject" title="choose_subject">
                                        <option value="partida">Info relativa a tu partida</option>                                    
                                        <option value="programacion">Contacta con nuestro dpto de programacion</option>
                                        <option value="Trabaja">Trabaja con nosotros</option>
                                        <option value="proyectos">Deseas proponernos proyectos</option>
                                        <option value="sugerencias">Haznos sugerencias</option>
                                        <option value="reclamaciones">Atendemos tus reclamaciones</option>
                                        <option value="club">Club nfu</option>
                                        <option value="sociales">Proyectos sociales</option>                                    
                                        <option value="actualizaciones">Te avisamos de nuestras actualizaciones</option>
                                        <option value="diferente">Algo diferente</option>
                                    </select>
                                </div> 
                                <br>                        
                                <div class="form-group">
                                    <label class="sr-only" for="contact-message">Mensaje</label>
                                    <textarea name="inputMessage" placeholder="Mensaje..." class="contact-message form-control" id="inputMessage"></textarea>
                                </div>                        
                                <button type="button" class="btn" name="submit" id="submitbtn">Enviar mensaje</button>
                                 <center><img src="<?php echo USERS_IMG_PATH; ?>ajax-loader.gif" alt="ajax loader icon" class="ajaxLoader" /><br/><br/>
                                 </center>
                                <input type="hidden" name="token" value="contact_form" />
                                <div id="message_alert" style="display: none;"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-5">
                      <div class="form-box">
              <div id="map"></div>
  
                
            </div>

        </div>


    </div>
    </div>
    </div>
