function validate_search(search_value) {
    if (search_value.length > 0) {
        var regexp = /^[a-zA-Z0-9 .]*$/;
        return regexp.test(search_value);
    }
    return false;
}
function refresh() {
    $('.pagination').html('');

    $('.pagination').val('');
}
function search_not_empty(keyword) {
    ////////////////////////// keyword.length >= 1 /////////////////////////
    $.post("/installation/num_pages_installation",{num_pages: true,keyword: keyword}, function (data, status) {
        var json = JSON.parse(data);
        //console.log(json);
        var pages = json.pages;
        // alert("num_pages_not_empty = " + pages);

        $("#results").load("/installation/obtain_installation", {keyword: keyword});

        if (pages !== 0) {
            refresh();
            $(".pagination").bootpag({
                total: pages,
                page: 1,
                maxVisible: 3,
                next: 'next',
                prev: 'prev'
            }).on("page", function (e, num) {

                e.preventDefault();
                $("#results").load("/installation/obtain_installation", {'page_num': num, 'keyword': keyword});
                reset();
            });
        } else {
            $("#results").load("/installation/view_error_false",{'view_error': false}); //view_error=false
            $('.pagination').html('');

            reset();
        }

        reset();

    }).fail(function (xhr) {
        $("#results").load("/installation/view_error_true",{'view_error': true});
        $('.pagination').html('');
        reset();
    });
}

function search_empty() {
    ////////////////////////// keyword.length == 0 /////////////////////////
    $.post("/installation/num_pages_installation",{num_pages: true}, function (data, status) {
     //alert("num_pages_empty = " + pages);
//alert(data);
        var json = JSON.parse(data);
        var pages = json.pages;
   //alert("num_pages_empty = " + pages);

     $("#results").load("/installation/obtain_installation"); //load initial records

        $(".pagination").bootpag({
            total: pages,
            page: 1,
            maxVisible: 3,
            next: 'next',
            prev: 'prev'
        }).on("page", function (e, num) {
            //alert(num);
            e.preventDefault();
            $("#results").load("/installation/obtain_installation", {'page_num': num});
            reset();
        });

        reset();

    }).fail(function (xhr) {
        $("#results").load("/installation/view_error_true",{'view_error': true});
        $('.pagination').html('');
        reset();
    });
}

function search_install(keyword) {
    //alert(keyword);
    $.post("/installation/nom_installation", {'nom_install': keyword}, function (data, status) {
        var json = JSON.parse(data);
        var install = json.install_autocomplete;
       // alert(json);
        //console.log(json);
        $('#results').html('');
        $('.pagination').html('');
        $('.thumbnail1').show();
           var cad =ruta_image(install[0].avatar);
       
        $('#img_install').html('<img id="img_install" src="' + cad + '"> ');
        $('#nom_install').html(install[0].nombre);
        $('#desc_install').html("Descripción: " + install[0].descripcion);
        $('#ub_install').html("Ubicación: " + install[0].ubicacion);
       var cad1 =ruta_image(install[0].valoracion);
              var cad1 =ruta_image(install[0].valoracion);
               var cad2=ruta_image("ubicacion.png");
            $('#valoration_install').html("Valoración:"+'<br>'+ '<img class="polaroids" src="'+ cad1 +'"> ');
             $('#ubicacion').html("<a target='_blank' href='http://maps.google.com/?q= "+install[0].latitud+" ,"+install[0].longitud+"'>"+'<img class="polaroids" src="'+ cad2 +'"> ');
//img.setAttribute("width","168"); 
//img.setAttribute("height","66"); 
        //var direccion="index.php?module=installation&view=list_installations";

    }).fail(function (xhr) {
        $("#results").load("/installation/view_error_false",{'view_error': false });
        $('.pagination').html('');
        reset();
    });
}

function count_install(keyword) {
    //alert(keyword);
    $.post("/installation/count_installation",{'count_install': keyword }, function (data, status) {
        var json = JSON.parse(data);
        var num_install = json.num_install;
        //alert(json);
       // console.log(json);

        if (num_install == 0) {
            $("#results").load("/installation/view_error_false",{'view_error': false }); //view_error=false
            $('.pagination').html('');
            reset();
        }
        if (num_install == 1) {
            //alert(keyword);
            search_install(keyword);
        }
        if (num_install > 1) {
            search_not_empty(keyword);
        }
    }).fail(function () {
        $("#results").load("/installation/view_error_false",{'view_error': false });//view_error=false
        $('.pagination').html('');
        $('.thumbnail1').hide();
        reset();
    });
}

function reset() {
    $('#img_install').html('');
    $('#nom_installt').html('');
    $('#desc_install').html('');
    $('#ub_install').html('');
    $('#valoration_install').html('');

    $('#keyword').val('');
}

$(document).ready(function () {

    if (!getCookie('keyword')) {
        search_empty();
    } else {
        //alert(getCookie('keyword'));
        count_install(getCookie('keyword'));
        $('#keyword').val(getCookie('keyword'));
        setCookie('keyword', '', 1);
    }
    ////////////////////////// inici carregar pàgina /////////////////////////


    $("#search_install").submit(function (e) {
        var keyword = document.getElementById('keyword').value;
        var v_keyword = validate_search(keyword);

        if (!v_keyword) {
           
            search_empty();
        } else {
            setCookie('keyword', keyword, 1);
             //alert(keyword);
             count_install(keyword);
            location.reload(true);
            //alert(keyword);
        }

        //si no ponemos la siguiente línea, el navegador nos redirecciona a index.php
        e.preventDefault(); //STOP default action
    });

    $('#Submit').click(function () {
        var keyword = document.getElementById('keyword').value;
        var v_keyword = validate_search(keyword);

        if (!v_keyword) {
            search_empty();
        } else {

            setCookie('keyword', keyword, 1);
            location.reload(true);
            //count_install(keyword);
            //alert(keyword);
        }
    });

    $.post("/installation/autocomplete_installation" ,{autocomplete: true}, function (data, status) {
        
   // console.log(data);

        var json = JSON.parse(data);   
        var nom_install = json.nom_install;
         //alert(nom_install[0].nombre);
       console.log(nom_install);

        var suggestions = new Array();
        for (var i = 0; i < nom_install.length; i++) {
            suggestions.push(nom_install[i].nombre);
        }
        //alert(suggestions);
        //console.log(suggestions);

        $("#keyword").autocomplete({
            source: suggestions,
            minLength: 1,
            select: function (event, ui) {
                // alert(ui.item.label);

                var keyword = ui.item.label;
                count_install(keyword);
            }
        });
    }).fail(function (xhr) {
        $("#results").load("/installation/view_error_false",{view_error: false}); //view_error=false
        $('.pagination').html('');
        reset();
    });

});



function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}


function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}

function ruta_image(image){
    
     alert(image);
    var project="http://51.254.97.198/NFU/media/"
    var ruta= project+image;
    return ruta;
}