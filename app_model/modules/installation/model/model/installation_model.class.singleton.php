<?php

//require(BLL_INSTALLATION . "installBLL.class.singleton.php");

class installation_model {

    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = installation_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    /* public function create_user($arrArgument) {
      return $this->bll->create_user_BLL($arrArgument);
      } */

    public function list_install() {

        return $this->bll->list_install_BLL();
    }

    public function details_install($id) {

        return $this->bll->details_install_BLL($id);
    }

    public function order_install($arrArgument) {

        return $this->bll->order_install_BLL($arrArgument);
    }

    public function count_install() {

        return $this->bll->count_install_BLL();
    }

    public function order_install_nombre() {

        return $this->bll->order_install_nombre_BLL();
    }

    public function filter_install_nombre($arrArgument) {

        return $this->bll->filter_install_nombre_BLL($arrArgument);
    }

    public function count_install_criteria($arrArgument) {

        return $this->bll->count_install__criteria_BLL($arrArgument);
    }

    public function order_install_criteria($arrArgument) {

        return $this->bll->order_install_criteria_BLL($arrArgument);
    }

}
