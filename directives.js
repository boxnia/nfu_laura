
//////////////////////////datepicker
app.directive('calendar', function () {
    return {
        require: 'ngModel',
        link: function (scope, el, attr, ngModel) {
            $(el).datepicker({
                dateFormat: 'mm-dd-yy',
                onSelect: function (dateText) {
                    scope.$apply(function () {
                        ngModel.$setViewValue(dateText);
                    });
                }
            });
        }
    };
})

/////////////////angular se utiliza camel case, en html hace referencia a ng-navside
//////////////////////////////pluguin menu
app.directive('ngNavside', function(){
   // console.log("navside")
    return{
        restric:'A',
        link: function(scope, element, attrs){
            $(element).navside(scope.$eval(attrs.ngNavside))
        }
    }
})
/////////////////////plugin carousel main
app.directive('ngCarousel', function(){
   // console.log("navside")
    return{
        restric:'A',
        link: function(scope, element, attrs){
            $(element).owlCarousel(scope.$eval(attrs.ngCarousel))
        }
    }
})


/////////////plugin las vegas main
app.directive('ngHeader', function(){
   // console.log("navside")
    return{
        restric:'A',
        link: function(scope, element, attrs){
            $(element).vegas(scope.$eval(attrs.ngHeader))
        }
    }
})
/////////////////////////validar password
app.directive('passwordCheck', [function () {
   return {
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            var firstPassword = '#' + attrs.passwordCheck;
              $(element).add(firstPassword).on('keyup', function () {
                scope.$apply(function () {
                    // console.info(elem.val() === $(firstPassword).val());
                    ctrl.$setValidity('pwmatch',   $(element).val() === $(firstPassword).val());
                });
            });
        }
    }
}]);
    
    ///////////////////////upload image
app.directive('dropZone', function() {
    
    
    return function(scope, element, attrs,$http) {
      
        $(element).dropzone({ 
        url:" http://51.254.97.198/NFU/app_model/index.php?module=users&function=upload_users",
        maxFilesize: 1000,
         params: {'upload': true},
        dictResponseError: "Ha ocurrido un error en el server",
        acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
        init: function() {
         
         // scope.files.push({file: 'added'});
          this.on('success', function(file, json) {
                 console.log(file)
             /* $("#progress").show();
                $("#bar").width('100%');
                $("#percent").html('100%');
                $('.msg').text('').removeClass('msg_error');
                $('#e_avatar').text('');
                $('.msg').text('Success Upload!!').addClass('msg_ok')//.animate({'right': '300px'}*/
          });
          
          this.on('addedfile', function(file) {
            scope.$apply(function(){
              alert(file);
              scope.files.push({file: 'added'});
            });
          });
          
          this.on('drop', function(file) {
            alert('file');
          });
          
        },
         complete: function (file) {
          
            if (file.status == "success") {
                alert("El archivo se ha subido correctamente: " + file.name);
            }
        },
        error: function (file) {
             alert("Error subiendo el archivo " + file.name);
        },
         removedfile: function (file, serverFileName) {
            var name = file.name;
            // alert(name);
            $.ajax({
                type: "POST",
               url:" http://51.254.97.198/NFU/app_model/index.php?module=users&function=upload_users",
                data: {'delete': true},
                ruta: "filename=" + name,
                success: function (ruta) {
                    //alert(ruta);
                    $("#progress").hide();
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    $("#e_avatar").html("");

                    var json = JSON.parse(ruta);
                    if (json.res === true) {
                        var element;
                        if ((element = file.previewElement) != null) {
                            element.parentNode.removeChild(file.previewElement);
                            //alert("Imagen eliminada: " + name);
                        } else {
                            false;
                        }
                    } else { //json.res == false, elimino la imagen también
                        var element;
                        if ((element = file.previewElement) != null) {
                            element.parentNode.removeChild(file.previewElement);
                        } else {
                            false;
                        }
                    }
                }
            });
        }
        
      });
     
      
     
      
      
    }
  });
  