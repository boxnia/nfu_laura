var app = angular.module('myApp', ['ngRoute', 'ui.bootstrap', 'ngCookies']);
//'ui.bootstrap' -> necesario para la paginación

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider

                // Home
                .when("/", {templateUrl: "modules/home/view/home.view.html", controller: "MainCtrl"})
                // Pages

                ///users 
                .when("/users", {templateUrl: "modules/users/view/users.view.html", controller: "UsersCtrl"})
                .when("/users_games", {templateUrl: "modules/users/view/users_games.view.html", controller: "UsersCtrl"})

                ///users admin
                .when("/admin", {templateUrl: "modules/users/view/admin.view.html", controller: "UsersCtrl"})
                .when("/admin_users_list", {templateUrl: "modules/users/view/admin_users.view.html", controller: "UsersCtrl"})

                ///login
                .when("/login", {templateUrl: "modules/login/view/login.view.html", controller: "LoginCtrl"})
                .when("/recovery", {templateUrl: "modules/login/view/recovery.view.html", controller: "LoginCtrl"})
                .when("/recovery_pass", {templateUrl: "modules/login/view/recovery_pass.view.html", controller: "LoginCtrl"})
                //installation
                .when("/install", {templateUrl: "modules/installation/view/list_install.view.html", controller: "InstallCtrl"})


                .when('/install_details/:installID', {
                    title: 'Details Install',
                    templateUrl: 'modules/installation/view/detail_install.view.html',
                    controller: 'DetailsInstallCtrl',
                    resolve: {
                        install: function (services, $route) {
                           
                            var installID = $route.current.params.installID;
                             console.log(installID);
                            return services.get('installation', 'idInstallation', installID);
                        }
                    }
                })

                //////games
                .when("/games_form", {templateUrl: "modules/games/view/gamesform.view.html", controller: "GamesCtrl"})
                ///contact
                .when("/contact", {templateUrl: "modules/contact/view/contact.view.html", controller: "ContactCtrl"})
                //.when("/faq", {templateUrl: "modules/inc/faq.view.html", controller: "PageCtrl"})
                //.when("/pricing", {templateUrl: "modules/inc/pricing.view.html", controller: "PageCtrl"})
                // .when("/services", {templateUrl: "modules/inc/services.view.html", controller: "PageCtrl"})
                // .when("/contact", {templateUrl: "modules/inc/contact.view.html", controller: "PageCtrl"})
                // Blog
                // .when("/blog", {templateUrl: "modules/blog/view/blog.view.html", controller: "BlogCtrl"})
                // .when("/blog/post", {templateUrl: "modules/blog/view/blog_item.view.html", controller: "BlogCtrl"})

                /* .when('/customers', {
                 title: 'Customers',
                 templateUrl: 'modules/customers/view/customers.view.html',
                 controller: 'listCtrl'
                 })
                 .when('/customers/edit-customer/:customerID', {
                 title: 'Edit Customers',
                 templateUrl: 'modules/customers/view/edit-customer.view.html',
                 controller: 'editCtrl',
                 resolve: {
                 customer: function(services, $route){
                 var customerID = $route.current.params.customerID;
                 return services.get('customer','customer', customerID);
                 }
                 }
                 })
                 
                 .when('/books', {
                 templateUrl: 'modules/books/view/libros-list.view.html',
                 controller: 'LibrosListController'
                 })
                 .when('/books/:bookId', {
                 templateUrl: 'modules/books/view/libro.view.html',
                 controller: 'LibroDetailController'
                 })
                 
                 
                 .when('/login', {
                 title: 'Login',
                 templateUrl: 'modules/login/view/login.view.html',
                 controller: 'authCtrl'
                 })
                 .when('/logout', {
                 title: 'Logout',
                 templateUrl: 'modules/login/view/login.view.html',
                 controller: 'authCtrl'
                 })
                 .when('/signup', {
                 title: 'Signup',
                 templateUrl: 'modules/login/view/signup.view.html',
                 controller: 'authCtrl'
                 })
                 .when('/dashboard', {
                 title: 'Dashboard',
                 templateUrl: 'modules/login/view/dashboard.view.html',
                 controller: 'authCtrl'
                 })*/


                // else 404
                .otherwise("/404", {templateUrl: "modules/inc/404.view.html", controller: "PageCtrl"});
    }])/*
     
     .run(function ($rootScope, $location, services) {
     $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
     $rootScope.title = current.$$route.title;
     });
     
     $rootScope.$on("$routeChangeStart", function (event, next, current) {
     $rootScope.authenticated = false;
     services.get('login','session')
     .then(function (results) {
     if (results.data.uid) {
     $rootScope.authenticated = true;
     $rootScope.uid = results.data.uid;
     $rootScope.name = results.data.name;
     $rootScope.email = results.data.email;
     }
     });
     });
     });
     */